FROM node:8-alpine

WORKDIR /

ADD package.json /package.json

RUN npm install

COPY .out/ /

EXPOSE 6006

CMD ["npm", "run", "storybook"]